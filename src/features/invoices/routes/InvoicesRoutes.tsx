import { FC } from "react";
import { Routes, Route } from "react-router-dom";

import { InvoiceDetailsPage, InvoiceFormPage, InvoicesPage } from "../pages";

export const InvoicesRoutes: FC = () => {
  return (
    <Routes>
      <Route path="/invoices">
        <Route index element={<InvoicesPage />} />
        <Route path="new" element={<InvoiceFormPage />} />
        <Route path=":id">
          <Route index element={<InvoiceDetailsPage />} />
          <Route path="edit" element={<InvoiceFormPage />} />
        </Route>
      </Route>
    </Routes>
  );
};
