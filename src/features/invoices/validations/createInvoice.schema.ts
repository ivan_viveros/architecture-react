import * as yup from "yup";

import { Invoice } from "../types";

export type CreateInvoiceSchema = Omit<Invoice, "id">;

export const createInvoiceSchema: yup.SchemaOf<CreateInvoiceSchema> = yup.object().shape({
  amount: yup.number().moreThan(0).required(),
  currency: yup.string().required(),
  description: yup.string().required(),
  issuedTo: yup.string().required(),
  title: yup.string().required(),
});
