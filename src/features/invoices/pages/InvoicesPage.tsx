import { Box, Button, Container, TextField, Typography } from "@mui/material";
import { useTheme } from "@mui/system";
import { ChangeEvent, FC } from "react";
import { Link, useSearchParams } from "react-router-dom";

import { useDebounce } from "~hooks";

import { useInvoicesQuery } from "../hooks";
import { InvoiceCard } from "../organisms";

export const InvoicesPage: FC = () => {
  const theme = useTheme();

  const [searchParams, setSearchParams] = useSearchParams();

  const searchQuery = searchParams.get("search");
  const debouncedSearchQuery = useDebounce(searchQuery || undefined);
  const limit = parseInt(searchParams.get("limit") || "2");
  const page = parseInt(searchParams.get("page") || "1");

  const { data: invoices, isLoading } = useInvoicesQuery({
    _limit: limit,
    _order: "desc",
    _page: page,
    _sort: "id",
    q: debouncedSearchQuery,
  });

  const handleSearchQuery = (e: ChangeEvent<HTMLInputElement>) => {
    setSearchParams({ page: "1", search: e.target.value });
  };

  const isFirstPage = page === 1;
  const isLastPage = invoices?.totalCount ? limit * page >= invoices?.totalCount : false;

  const handlePrevPage = () => {
    const previousPage = (page - 1).toString();
    setSearchParams(searchQuery ? { page: previousPage, search: searchQuery } : { page: previousPage });
  };

  const handleNextPage = () => {
    const nextPage = (page + 1).toString();
    setSearchParams(searchQuery ? { page: nextPage, search: searchQuery } : { page: nextPage });
  };

  return (
    <Container sx={{ my: theme.spacing(4) }}>
      <Typography variant="h4" sx={{ mb: theme.spacing(1) }}>
        Invoices
      </Typography>
      <Box sx={{ alignItems: "center", display: "flex", justifyContent: "space-between", mb: theme.spacing(4) }}>
        <TextField size="small" placeholder="Search invoices" onChange={handleSearchQuery} value={searchQuery || ""} />
        <Button disableElevation component={Link} to="new" variant="contained" color="primary">
          Create an invoice
        </Button>
      </Box>
      <Box
        sx={{
          display: "grid",
          gap: theme.spacing(2),
          gridTemplateColumns: "repeat(auto-fill, minmax(300px, 1fr))",
          mb: theme.spacing(2),
        }}
      >
        {isLoading && <Typography>Loading invoices...</Typography>}
        {invoices && invoices.data.length === 0 && <Typography>No invoices</Typography>}
        {invoices &&
          invoices.data.length > 0 &&
          invoices.data.map((invoice) => <InvoiceCard key={invoice.id} invoice={invoice} />)}
      </Box>
      <Box sx={{ alignItems: "center", display: "flex", justifyContent: "space-between" }}>
        <Button disabled={isLoading || isFirstPage} variant="outlined" onClick={handlePrevPage}>
          Prev
        </Button>
        <Button disabled={isLoading || isLastPage} variant="outlined" onClick={handleNextPage}>
          Next
        </Button>
      </Box>
    </Container>
  );
};
