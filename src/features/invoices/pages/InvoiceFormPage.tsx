import { Container, Typography } from "@mui/material";
import { FC } from "react";
import { useNavigate, useParams } from "react-router-dom";

import { useCreateInvoiceMutation, useInvoiceQuery, useUpdateInvoiceMutation } from "../hooks";
import { InvoiceForm } from "../organisms";
import { CreateInvoiceSchema } from "../validations";

export const InvoiceFormPage: FC = () => {
  const navigate = useNavigate();

  const params = useParams();
  const invoiceIdToEdit = params.id ? parseInt(params.id as string) : undefined;

  const { data: invoiceToEdit, isLoading: isLoadingInvoiceData } = useInvoiceQuery(invoiceIdToEdit);
  const { mutate: createInvoice, isLoading: isCreating } = useCreateInvoiceMutation();
  const { mutate: updateInvoice, isLoading: isUpdating } = useUpdateInvoiceMutation();

  const handleSubmit = (formData: CreateInvoiceSchema) => {
    const onSuccess = () => navigate("/invoices");

    if (!invoiceIdToEdit) return createInvoice(formData, { onSuccess });

    const noDataChanged = JSON.stringify(formData) === JSON.stringify(invoiceToEdit);
    if (noDataChanged) return onSuccess();

    return updateInvoice({ id: invoiceIdToEdit, ...formData }, { onSuccess });
  };

  if (invoiceIdToEdit && isLoadingInvoiceData) {
    return <Typography>Loading....</Typography>;
  }

  return (
    <Container sx={{ my: "1rem" }}>
      <Typography variant="h4" component="h1" sx={{ mb: "1rem" }}>
        {invoiceIdToEdit ? "Edit invoice" : "Create invoice"}
      </Typography>
      <InvoiceForm
        defaultValues={invoiceIdToEdit ? invoiceToEdit : { currency: "MXN" }}
        onSubmit={handleSubmit}
        buttonLabel={invoiceIdToEdit ? "Update" : "Create"}
        isSubmitting={invoiceIdToEdit ? isUpdating : isCreating}
      />
    </Container>
  );
};
