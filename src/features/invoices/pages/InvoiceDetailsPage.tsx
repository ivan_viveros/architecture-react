import { Box, Button, CircularProgress, Container, Typography, useTheme } from "@mui/material";
import { FC } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";

import { ApiError } from "~api";

import { useDeleteInvoiceMutation, useInvoiceQuery } from "../hooks";

export const InvoiceDetailsPage: FC = () => {
  const theme = useTheme();

  const navigate = useNavigate();

  const params = useParams();
  const invoiceId = parseInt(params.id as string);

  const { data: invoice, isLoading, error } = useInvoiceQuery(invoiceId);
  const { mutate: deleteInvoice, isLoading: isDeleting } = useDeleteInvoiceMutation();

  const handleDelete = () => {
    const isSure = confirm("Are your sure?");
    if (!isSure) return;

    const onSuccess = () => navigate("/invoices");
    deleteInvoice({ id: invoiceId }, { onSuccess });
  };

  if (isLoading)
    return (
      <Box display="flex" alignItems="center" justifyContent="center" minHeight="100vh">
        <CircularProgress />
      </Box>
    );

  if ((error as ApiError)?.status === 404) return <Typography>Not Found</Typography>;

  return (
    <Container sx={{ my: theme.spacing(4) }}>
      <Typography variant="h4" component="h1" sx={{ mb: theme.spacing(1) }}>
        {invoice?.title}
      </Typography>
      <Typography sx={{ mb: theme.spacing(2) }}>{invoice?.description}</Typography>
      <Button
        disableElevation
        sx={{ mr: theme.spacing(2) }}
        variant="contained"
        color="warning"
        component={Link}
        to="edit"
      >
        Edit
      </Button>
      <Button disableElevation variant="contained" color="error" onClick={handleDelete} disabled={isDeleting}>
        {isDeleting ? "Deleting" : "Delete"}
      </Button>
    </Container>
  );
};
