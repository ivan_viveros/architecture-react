import { Avatar, Box, Link as MuiLink, Typography, useTheme } from "@mui/material";
import { FC } from "react";
import { Link } from "react-router-dom";

import { Invoice } from "../types";

export interface InvoiceCardProps {
  invoice: Invoice;
}

export const InvoiceCard: FC<InvoiceCardProps> = ({ invoice }) => {
  const theme = useTheme();

  return (
    <Box
      sx={{ border: `1px solid ${theme.palette.divider}`, borderRadius: theme.shape.borderRadius, p: theme.spacing(2) }}
    >
      <Box sx={{ display: "flex", justifyContent: "space-between", mb: theme.spacing(1) }}>
        <MuiLink component={Link} to={`${invoice.id}`}>
          {invoice.title}
        </MuiLink>
        <Typography>
          {new Intl.NumberFormat("es-MX", {
            currency: invoice.currency,
            style: "currency",
          }).format(invoice.amount)}
        </Typography>
      </Box>
      <Typography component="p" variant="body2" color="GrayText">
        {invoice.description}
      </Typography>
      <Avatar sx={{ height: theme.spacing(4), mt: theme.spacing(2), width: theme.spacing(4) }}>
        {invoice.issuedTo.charAt(0)}
      </Avatar>
    </Box>
  );
};
