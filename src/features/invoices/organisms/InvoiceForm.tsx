import { yupResolver } from "@hookform/resolvers/yup";
import { Box, Button, TextField } from "@mui/material";
import { FC } from "react";
import { useForm } from "react-hook-form";

import { createInvoiceSchema, CreateInvoiceSchema } from "../validations";

interface InvoiceFormProps {
  buttonLabel: string;
  defaultValues?: Partial<CreateInvoiceSchema>;
  isSubmitting: boolean;
  onSubmit: (data: CreateInvoiceSchema) => void;
}

export const InvoiceForm: FC<InvoiceFormProps> = ({ buttonLabel, onSubmit, defaultValues, isSubmitting }) => {
  const { handleSubmit, register } = useForm<CreateInvoiceSchema>({
    defaultValues,
    resolver: yupResolver(createInvoiceSchema),
  });

  return (
    <Box
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      sx={{ display: "grid", gap: "1rem", gridTemplateColumns: "1fr" }}
    >
      <TextField {...register("title")} label="Title" />
      <TextField {...register("description")} label="Description" />
      <TextField {...register("amount")} label="Amount" />
      <TextField {...register("issuedTo")} label="To" />
      <Button disabled={isSubmitting} variant="contained" color="primary" size="large" disableElevation type="submit">
        {buttonLabel}
      </Button>
    </Box>
  );
};
