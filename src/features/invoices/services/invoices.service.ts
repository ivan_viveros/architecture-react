import { api, ApiError, ApiParams } from "~api";

import { Invoice } from "../types";

const BASE_ENDPOINT = "/invoices";

export interface GetInvoicesQueryParams extends ApiParams, Partial<Invoice> {}

export const getInvoices = async (queryParams?: GetInvoicesQueryParams) => {
  const response = await api.get<Invoice[]>(BASE_ENDPOINT, queryParams);
  if (!response.ok) throw new ApiError(response.problem, response.status!);
  return { data: response.data!, totalCount: parseInt(response.headers!["x-total-count"]) };
};

export type GetInvoiceParams = Pick<Invoice, "id">;

export const getInvoice = async ({ id }: GetInvoiceParams) => {
  const response = await api.get<Invoice>(`${BASE_ENDPOINT}/${id}`);
  if (!response.ok) throw new ApiError(response.problem, response.status!);
  return response.data;
};

export type CreateInvoiceParams = Omit<Invoice, "id">;

export const createInvoice = async (payload: CreateInvoiceParams) => {
  const response = await api.post<Invoice>(BASE_ENDPOINT, payload);
  if (!response.ok) throw new ApiError(response.problem, response.status!);
  return response.data!;
};

export interface UpdateInvoiceParams extends Partial<Omit<Invoice, "id">>, Pick<Invoice, "id"> {}

export const updateInvoice = async ({ id, ...payload }: UpdateInvoiceParams) => {
  const response = await api.patch<Invoice>(`${BASE_ENDPOINT}/${id}`, payload);
  if (!response.ok) throw new ApiError(response.problem, response.status!);
  return response.data!;
};

export type DeleteInvoiceParams = Pick<Invoice, "id">;

export const deleteInvoice = async ({ id }: DeleteInvoiceParams) => {
  const response = await api.delete<Invoice>(`${BASE_ENDPOINT}/${id}`);
  if (!response.ok) throw new ApiError(response.problem, response.status!);
  return response.data!;
};
