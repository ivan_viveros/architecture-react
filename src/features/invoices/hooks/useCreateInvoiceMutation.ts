import { useMutation, useQueryClient } from "react-query";

import { invoicesService } from "../services";

export const useCreateInvoiceMutation = () => {
  const queryClient = useQueryClient();

  return useMutation(invoicesService.createInvoice, {
    onSuccess: () => {
      queryClient.invalidateQueries("invoices");
    },
  });
};
