import { useQuery } from "react-query";

import { invoicesService } from "../services";

export const useInvoiceQuery = (id: number | undefined) => {
  return useQuery(["invoices", id], () => invoicesService.getInvoice({ id: id! }), {
    enabled: !!id,
  });
};
