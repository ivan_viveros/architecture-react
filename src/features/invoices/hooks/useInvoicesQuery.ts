import { useQuery } from "react-query";

import { invoicesService } from "../services";

export const useInvoicesQuery = (queryParams?: invoicesService.GetInvoicesQueryParams) => {
  return useQuery(["invoices", queryParams], () => invoicesService.getInvoices(queryParams));
};
