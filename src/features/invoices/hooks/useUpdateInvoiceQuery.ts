import { useMutation, useQueryClient } from "react-query";

import { invoicesService } from "../services";

export const useUpdateInvoiceMutation = () => {
  const queryClient = useQueryClient();

  return useMutation(invoicesService.updateInvoice, {
    onSuccess: (invoice) => {
      queryClient.invalidateQueries(["invoices", invoice.id]);
      queryClient.invalidateQueries("invoices");
    },
  });
};
