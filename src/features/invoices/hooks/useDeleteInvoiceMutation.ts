import { useMutation, useQueryClient } from "react-query";

import { invoicesService } from "../services";

export const useDeleteInvoiceMutation = () => {
  const queryClient = useQueryClient();

  return useMutation(invoicesService.deleteInvoice, {
    onSuccess: () => {
      queryClient.invalidateQueries("invoices");
    },
  });
};
