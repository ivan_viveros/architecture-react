export interface Invoice {
  amount: number;
  currency: string;
  description: string;
  id: number;
  issuedTo: string;
  title: string;
}
