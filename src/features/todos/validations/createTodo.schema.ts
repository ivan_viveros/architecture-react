import * as yup from "yup";

import { Todo } from "../types";

export type CreateTodoSchema = Pick<Todo, "title">;

export const createTodoSchema: yup.SchemaOf<CreateTodoSchema> = yup.object().shape({
  title: yup.string().required(),
});
