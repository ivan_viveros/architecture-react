import { FC } from "react";
import { Route, Routes } from "react-router-dom";

import { TodoDetailsPage, TodosPage } from "../pages";

const TodosRoutes: FC = () => {
  return (
    <Routes>
      <Route path="/todos">
        <Route index element={<TodosPage />} />
        <Route path=":id" element={<TodoDetailsPage />} />
      </Route>
    </Routes>
  );
};

export default TodosRoutes;
