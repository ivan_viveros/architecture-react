import { api, ApiError, ApiParams } from "~api";

import { Todo } from "../types";

const BASE_ENDPOINT = "/todos";

export interface GetTodosQueryParams extends ApiParams, Partial<Todo> {}

export const getTodos = async (params?: GetTodosQueryParams): Promise<Todo[]> => {
  const response = await api.get<Todo[]>(BASE_ENDPOINT, params);
  if (!response.ok) throw new ApiError(response.problem, response.status!);
  return { data: response.data!, total: response.headers["X-Total-Count"] };
};

export type GetTodoParams = Pick<Todo, "id">;

export const getTodo = async ({ id }: GetTodoParams): Promise<Todo> => {
  const response = await api.get<Todo>(`${BASE_ENDPOINT}/${id}`);
  if (!response.ok) throw new ApiError(response.problem, response.status!);
  return response.data!;
};

type CreateTodoPayload = Omit<Todo, "id">;

export const createTodo = async (payload: CreateTodoPayload): Promise<Todo> => {
  const response = await api.post<Todo, Omit<Todo, "id">>(BASE_ENDPOINT, payload);
  if (!response.ok) throw new ApiError(response.problem, response.status!);
  return response.data!;
};

export interface UpdateTodoPayload extends Partial<Omit<Todo, "id">>, Pick<Todo, "id"> {}

export const updateTodo = async ({ id, ...payload }: UpdateTodoPayload): Promise<Todo> => {
  const response = await api.patch<Todo>(`${BASE_ENDPOINT}/${id}`, payload);
  if (!response.ok) throw new ApiError(response.problem, response.status!);
  return response.data!;
};

export type DeleteTodoPayload = Pick<Todo, "id">;

export const deleteTodo = async ({ id }: DeleteTodoPayload) => {
  const response = await api.delete<Todo>(`${BASE_ENDPOINT}/${id}`);
  if (!response.ok) throw new ApiError(response.problem, response.status!);
  return response.data!;
};
