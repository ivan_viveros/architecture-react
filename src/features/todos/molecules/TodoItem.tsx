import { FC } from "react";
import { Link } from "react-router-dom";

import { useDeleteTodoMutation, useUpdateTodoMutation } from "../hooks";
import { Todo } from "../types";

interface TodoItemProps {
  todo: Todo;
}

export const TodoItem: FC<TodoItemProps> = ({ todo }) => {
  const { mutate: updateTodo } = useUpdateTodoMutation();
  const { mutate: deleteTodo } = useDeleteTodoMutation();

  const handleToggleCheckbox = () => updateTodo({ ...todo, isCompleted: !todo.isCompleted });
  const handleDelete = () => deleteTodo(todo);

  return (
    <label htmlFor={todo.id.toString()}>
      <input type="checkbox" checked={todo.isCompleted} onChange={handleToggleCheckbox} id={todo.id.toString()} />
      <span>{todo.title}</span>
      <Link to={`${todo.id}/edit`}>Edit</Link>
      <button onClick={handleDelete}>Delete</button>
    </label>
  );
};
