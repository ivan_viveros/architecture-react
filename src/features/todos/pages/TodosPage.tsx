import { yupResolver } from "@hookform/resolvers/yup";
import { ChangeEvent, FC } from "react";
import { useForm } from "react-hook-form";
import { useSearchParams } from "react-router-dom";

import { useDebounce } from "~hooks";

import { useCreateTodoMutation, useTodosQuery } from "../hooks";
import { TodoList } from "../organisms";
import { CreateTodoSchema, createTodoSchema } from "../validations";

export const TodosPage: FC = () => {
  const [searchParams, setSearchParams] = useSearchParams();

  const { register, handleSubmit } = useForm<CreateTodoSchema>({
    resolver: yupResolver(createTodoSchema),
  });

  const searchQuery = searchParams.get("search");
  const debouncedSearchQuery = useDebounce(searchQuery || undefined);
  const limit = parseInt(searchParams.get("limit") || "10");
  const page = parseInt(searchParams.get("page") || "1");

  const {
    data: todos,
    error: todosError,
    isLoading: isLoadingTodos,
  } = useTodosQuery({ _limit: limit, _page: page, q: debouncedSearchQuery });

  const { mutate: createTodo, isLoading: isCreatingTodo } = useCreateTodoMutation();

  const handleSearchQuery = (e: ChangeEvent<HTMLInputElement>) => {
    setSearchParams({ page: "1", search: e.target.value });
  };

  const onSubmit = (data: CreateTodoSchema) => {
    return createTodo({ ...data, isCompleted: false });
  };

  return (
    <div>
      <input type="text" placeholder="Search todos" value={searchQuery || ""} onChange={handleSearchQuery} />
      {todosError && <p>An error ocurred while getting the most recent todos</p>}
      {isLoadingTodos && <p>Loading todos...</p>}
      {todos && todos.length === 0 && <p>There are no todos!</p>}
      {todos && todos.length > 0 && <TodoList todos={todos} />}
      <form onSubmit={handleSubmit(onSubmit)}>
        <input type="text" {...register("title")} />
        <button type="submit" disabled={isCreatingTodo}>
          Add
        </button>
      </form>
    </div>
  );
};
