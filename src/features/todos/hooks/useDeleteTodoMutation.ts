import { useMutation, useQueryClient } from "react-query";

import { todosService } from "../services";

export const useDeleteTodoMutation = () => {
  const queryClient = useQueryClient();

  return useMutation(todosService.deleteTodo, {
    onSuccess: (todo) => {
      queryClient.invalidateQueries(["todos", todo.id]);
    },
  });
};
