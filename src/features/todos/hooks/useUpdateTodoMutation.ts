import { useMutation, useQueryClient } from "react-query";

import { todosService } from "../services";

export const useUpdateTodoMutation = () => {
  const queryClient = useQueryClient();

  return useMutation(todosService.updateTodo, {
    onSuccess: (todo) => {
      queryClient.invalidateQueries(["todos", todo.id]);
    },
  });
};
