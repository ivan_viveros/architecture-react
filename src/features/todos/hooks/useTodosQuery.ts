import { useQuery } from "react-query";

import { todosService } from "../services";

export const useTodosQuery = (params?: todosService.GetTodosQueryParams) => {
  return useQuery(["todos", params], () => todosService.getTodos(params));
};
