import { useMutation, useQueryClient } from "react-query";

import { todosService } from "../services";

export const useCreateTodoMutation = () => {
  const queryClient = useQueryClient();

  return useMutation(todosService.createTodo, {
    onSuccess: () => {
      queryClient.invalidateQueries("todos");
    },
  });
};
