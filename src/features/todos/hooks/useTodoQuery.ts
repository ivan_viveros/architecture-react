import { useQuery } from "react-query";

import { todosService } from "../services";

export const useTodoQuery = (id: number | undefined) => {
  return useQuery(["todos", id], () => todosService.getTodo({ id: id! }), { enabled: !!id });
};
