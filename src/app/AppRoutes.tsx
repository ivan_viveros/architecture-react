import { FC } from "react";
import { BrowserRouter } from "react-router-dom";

import { InvoicesRoutes } from "~features/invoices/routes";

const AppRoutes: FC = () => {
  return (
    <BrowserRouter>
      <InvoicesRoutes />
    </BrowserRouter>
  );
};

export default AppRoutes;
