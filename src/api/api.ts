import { create } from "apisauce";

export const api = create({ baseURL: import.meta.env.VITE_API_URL });

export interface ApiParams {
  [key: string]: unknown;
  _limit?: number;
  _order?: string;
  _page?: number;
  _sort?: string;
  q?: string;
}

export class ApiError extends Error {
  constructor(public problem: string, public status: number) {
    super(problem);
    this.status = status;
  }
}
